package Main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class FileMapCache {
	private File mapcacheindex = new File(System.getProperty("java.io.tmpdir") + System.getProperty("file.separator") + "MapCacheIndex.data");
	//List<IndexItem> index = null;
	boolean index[][][] = null;
	
	public FileMapCache() {
	      if(!mapcacheindex.exists()) {
	         saveIndexCache(mapcacheindex);
	      } else {
	           loadIndexCache(mapcacheindex);
	      }
	}

	private boolean saveIndexCache(File save) {
        try {
        	//if(index == null) index = new boolean[18][131072][131072];
            FileOutputStream fos = new FileOutputStream(save);
            GZIPOutputStream gzos = new GZIPOutputStream(fos);
            ObjectOutputStream out = new ObjectOutputStream(gzos);
            out.writeObject(index);
            out.flush();
            out.close();
            return true;
         }
         catch (IOException e) {
           return false;
         }
	}
		
	private boolean loadIndexCache(File load) {
        try {
            FileInputStream fis = new FileInputStream(load);
            GZIPInputStream gzis = new GZIPInputStream(fis);
            ObjectInputStream in = new ObjectInputStream(gzis);
            index = (boolean[][][])in.readObject();
            in.close();
            return true;
          }
          catch (Exception e) {
              return false;
          }
	}

	public Icon getImage(int x, int y, int z) {
		
		if(cacheLookup(x, y, z)) return getCacheImage(x, y, z);
		else {
			try {
				int server = (int)(Math.random() * 4);
				return new ImageIcon(ImageIO.read(new URL("http://mt" + server + ".google.com/mt?n=404&v=w2.39&x=" + (x + 191) + "&y=" + (y + 382) + "&zoom=7")));
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
	}
	
	private Icon getCacheImage(int x, int y, int z) {
		return null;
	}

	public boolean cacheLookup(int x, int y, int z) {
		return false;
	}

}
