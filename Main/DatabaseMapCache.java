package Main;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.Raster;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Vector;
import java.util.zip.ZipException;

import javax.swing.ImageIcon;

public class DatabaseMapCache extends Thread {
	//private File mapcacheindex = new File(System.getProperty("java.io.tmpdir") + System.getProperty("file.separator") + "MapCacheIndex.data");
	//List<IndexItem> index = null;
	boolean index[][][] = null;
	String connection_info[] = new String[4];
	MySQLDatabase md = null;
	Vector<WorkingPoint> v = new Vector<WorkingPoint>(10000);
	
	public DatabaseMapCache() {
		connection_info[0] = "192.168.5.1";
		connection_info[1] = "NiloGPS";
		connection_info[2] = "root";
		connection_info[3] = "";
		md = new MySQLDatabase(connection_info, 0);
		md.connect();
	}

	public synchronized void setImage(int x, int y, int z, GPSLabel label) {
		v.addElement(new WorkingPoint(x, y, z, label));
		//System.out.println("Sending Request: " + x);
	}
	
	public void run() {
		while(true) {
			
			if(v.isEmpty()) {
				try {
					Thread.sleep(500);
					//System.out.println("THis is the run running.");
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				WorkingPoint w = v.remove(0);
				//System.out.println("We have working point");
				System.out.println("Getting Cache for: X: " + w.getX() + " Y: " + w.getY() + " Z: " + w.getZ());
				byte[] buffer = md.getCacheImage(w.getX(), w.getY(), w.getZ());
			
				if(buffer == null) {
					try {
						int server = (int)(Math.random() * 4);
						URL url = new URL("http://mt" + server + ".google.com/mt?n=404&v=w2.39&x=" + w.getX() + "&y=" + w.getY() + "&zoom=" + w.getZ());
						InputStream fi = url.openStream();
						//System.out.println(url.toString());
						buffer = new byte[fi.available()];
						fi.read(buffer);
						fi.close();


						//ImageIcon ic = new ImageIcon(url);

						
						System.out.println("Grabbing: " + url.toString());
						//InputStream in = url.openStream();
						//buffer = new byte[65000];
						//int len = in.read(buffer);
						//in.close();
						//byte[] buff2 = new byte[len];
						//for(int i = 0; i < len; i++) { buff2[i] = buffer[i]; }
						//buff2 = buffer;
						//System.out.println(buffer + " Len: " + len);
						//System.out.println(url.openStream());
						//ic = new ImageIcon(buffer);
						//ic = new ImageIcon(ImageIO.read(url));
						//System.out.println(url.getContent());
						md.saveCache(w.getX(), w.getY(), w.getZ(), buffer);
						w.getGl().setIcon(new ImageIcon(buffer));
					} catch (MalformedURLException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} else {
					ImageIcon ic = null;
					try {
					ic = new ImageIcon(buffer);
					} catch (Exception e) {
						System.out.println(ic.getImageLoadStatus());
						System.out.println("We have a data problem");
					}

					w.getGl().setIcon(ic);
				}
			}
		}
	}
}
