package Main;

public class WorkingPoint {
	private int x = 0;
	private int y = 0;
	private int z = 0;
	private GPSLabel gl = null;
	public WorkingPoint(int X, int Y, int Z, GPSLabel LB) {
		x = X; y = Y; z = Z; gl = LB;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getZ() {
		return z;
	}
	public void setZ(int z) {
		this.z = z;
	}
	public GPSLabel getGl() {
		return gl;
	}
	public void setGl(GPSLabel gl) {
		this.gl = gl;
	}
	
	public String toString() {
		return "X: " + x + " Y: " + y + " Z: " + z;
	}

}
