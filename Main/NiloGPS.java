package Main;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.border.LineBorder;

public class NiloGPS extends JFrame {

	private static final long serialVersionUID = 1L;
	private int lw = 8;
	private int lh = 8;
	//private int z = 0;
	private int startX = 26558;
	private int startY = 50895;
	private int tilesize = 256;
	private int keyshiftamount = 25;
	private int pagedownshiftamount = keyshiftamount * 5;
	private GPSLabel[][] labels = new GPSLabel[lw][lh];
	//private int[][] locations = new int[lw][lh];
	//private GPSLabel gp = new GPSLabel(0, 0);
	private int offsetx = 0;
	private int offsety = 0;
	// http://mt0.google.com/mt?n=404&v=w2.39&x=3&y=5&zoom=13

	public NiloGPS() {
		super.setName("NiloGPS");
		this.addMouseListener(
				new MouseListener() {
					public void mouseClicked(MouseEvent e) {
						if(e.getButton() == 3) {
							//System.out.println(e.getButton());
						}
					}
					public void mouseEntered(MouseEvent e) {}
					public void mouseExited(MouseEvent e) {}
					public void mouseReleased(MouseEvent e) {}
					public void mousePressed(MouseEvent e) {
						offsetx = e.getX();
						offsety = e.getY();
					}
				}
		);
		this.addMouseMotionListener(
			new MouseMotionListener() {
				public void mouseDragged(MouseEvent e) {
					moveLabels((e.getX() - offsetx), (e.getY() - offsety));
					offsetx = e.getX();
					offsety = e.getY();
				}
				public void mouseMoved(MouseEvent e) {}
			}
		);
		this.addMouseWheelListener(
			new MouseWheelListener() {
				public void mouseWheelMoved(MouseWheelEvent e) {
					//z += e.getWheelRotation();
					//System.out.println("Z: " + z);
					if(e.getWheelRotation() < 0) {
						zoomIn();
					} else {
						zoomOut();
					}
				}
			}
		);
		this.addKeyListener(
			new KeyListener() {
				public void keyPressed(KeyEvent e) {
					if(e.getKeyCode() == 33) { moveLabels(0, pagedownshiftamount); }
					if(e.getKeyCode() == 34) { moveLabels(0, -pagedownshiftamount); }
					if(e.getKeyCode() == 37) { moveLabels(keyshiftamount, 0); }
					if(e.getKeyCode() == 39) { moveLabels(-keyshiftamount, 0); }
					if(e.getKeyCode() == 38) { moveLabels(0, keyshiftamount); }
					if(e.getKeyCode() == 40) { moveLabels(0, -keyshiftamount); }
				}
				public void keyReleased(KeyEvent e) {}
				public void keyTyped(KeyEvent e) {}
			}
		);
	}
	
	public void createLabels() {

	}
	
	public void moveLabels(int x, int y) {
		//System.out.println("X: " + x + " Y: " + y);
		for(int w = 0; w < lw; w++) {
			for(int h = 0; h < lh; h++) {
				if(labels[w][h].getX() < -tilesize && x < 0) {
					labels[w][h].setLocation(labels[w][h].getX() + (lw * tilesize), labels[w][h].getY());
					labels[w][h].setLabelLocation(labels[w][h].getXL() + lw, labels[w][h].getYL());
				} else if(labels[w][h].getY() < -tilesize && y < 0) {
					labels[w][h].setLocation(labels[w][h].getX(), labels[w][h].getY() + (lh * tilesize));
					labels[w][h].setLabelLocation(labels[w][h].getXL(), labels[w][h].getYL() + lh);
				} else if(labels[w][h].getX() > this.getWidth() && x > 0)  {
					labels[w][h].setLocation(labels[w][h].getX() - (lw * tilesize), labels[w][h].getY());
					labels[w][h].setLabelLocation(labels[w][h].getXL() - lw, labels[w][h].getYL());
				} else if(labels[w][h].getY() > this.getHeight() && y > 0) {
					labels[w][h].setLocation(labels[w][h].getX(), labels[w][h].getY() - (lh * tilesize));
					labels[w][h].setLabelLocation(labels[w][h].getXL(), labels[w][h].getYL() - lh);
				}
				labels[w][h].setLocation(labels[w][h].getX() + x, labels[w][h].getY() + y);
			}
		}
	}
	
	public void init() {
		DatabaseMapCache mc = new DatabaseMapCache();
		mc.start();
		for(int w = 0; w < lw; w++) {
			for(int h = 0; h < lh; h++) {
				labels[w][h] = new GPSLabel(w + startX, h + startY, mc);
				labels[w][h].setBounds(w*tilesize, h*tilesize, tilesize, tilesize);
				labels[w][h].setLabelLocation(w + startX, h + startY);
				labels[w][h].setBorder(new LineBorder(new Color(0,0,0)));
				this.add(labels[w][h]);
			}
		}
		this.add(new JLabel());
	}
	
	public void zoomIn() {
		for(int x = 0; x < lw; x++)  {
			for(int y = 0; y < lh; y++) {
				labels[x][y].zoomIn();
			}
		}
	}
	
	public void zoomOut() {
		for(int x = 0; x < lw; x++)  {
			for(int y = 0; y < lh; y++) {
				labels[x][y].zoomOut();
			}
		}
	}
	
	public static void main(String args[]) {
		NiloGPS ng = new NiloGPS();
		ng.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ng.setLocationRelativeTo(null);
		ng.setSize(800, 800);
		ng.setVisible(true);
        ng.init();
	}
}
