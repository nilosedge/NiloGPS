package Main;

import javax.swing.JLabel;

public class GPSLabel extends JLabel {
		private static final long serialVersionUID = 1L;
		private int locx = 0;
		private int locy = 0;
		private int zoom = 0;
		DatabaseMapCache mc = null;
		
		GPSLabel(int X, int Y, DatabaseMapCache MC) {
			locx = X;
			locy = Y;
			mc = MC;
		}
		
		void setLabelLocation(int X, int Y) {
			this.setText("W: " + X + ", " + " H: " + Y);
			mc.setImage(X, Y, zoom, this);
			//System.out.println("X: " + X + " Y: " + Y + " Z: " + zoom);
			locx = X;
			locy = Y;
		}
		int getXL() { return locx; }
		int getYL() { return locy; }

		public void zoomIn() {
			if(zoom != 0) {
				zoom--;
				locx *= 2;
				locy *= 2;
			}
		}

		public void zoomOut() {
			if(zoom != 18) {
				zoom++;
				locx /= 2;
				locy /= 2;
			}
		}
}
