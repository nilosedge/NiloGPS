package Main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MySQLDatabase {
	private Connection conn = null;
	private String dbhost = null;
	private String dbname = null;
	private String dbuser = null;
	private String dbpass = null;
	private int debug = 0;
	
	public MySQLDatabase(String con_info[], int debug) {
		dbhost = con_info[0];
		dbname = con_info[1];
		dbuser = con_info[2];
		dbpass = con_info[3];
		this.debug = debug;
	}
	
	public void set_info(String con_info[], int debug) {
		dbhost = con_info[0];
		dbname = con_info[1];
		dbuser = con_info[2];
		dbpass = con_info[3];
		this.debug = debug;
	}

	public boolean connect() {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection("jdbc:mysql://" + dbhost + "/" + dbname, dbuser, dbpass);
			if(!conn.isClosed() && debug > 0) System.out.println("Successfully connected to: " + dbhost + " using db " + dbname);
			return true;
		} catch(Exception e) {
			System.err.println("Exception: " + e.getMessage());
			return false;
		}
	}
	
	public ResultSet query(String query) {
		if(debug > 0) System.out.println(query);
		Statement stmt;
		try {
			stmt = conn.createStatement();
			ResultSet res = null;
			if(query.toLowerCase().substring(0,6).equals("select")) {
				res = stmt.executeQuery(query);
			} else {
				stmt.executeUpdate(query);
			}
			return res;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public byte[] getCacheImage(int x, int y, int z) {
		try {
			PreparedStatement ps = conn.prepareStatement("select * from MapCache where x=? and y=? and z=?");
			ps.setInt(1, x);
			ps.setInt(2, y);
			ps.setInt(3, z);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				byte[] buffer = (byte [])rs.getObject("image");
				rs.close();
				ps.close();
				return buffer;
			} else {
				return null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void saveCache(int x, int y, int z, byte[] buffer) {
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement("insert into MapCache (x, y, z, image) values(?, ?, ?, ?)");
			ps.setInt(1, x);
			ps.setInt(2, y);
			ps.setInt(3, z);
			ps.setBytes(4, buffer);
			//System.out.println(ps.getWarnings());
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
	}
}

